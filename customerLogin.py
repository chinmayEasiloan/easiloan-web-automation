#! python3
import pyautogui, sys
#import requests
import time

def openChromeWindow():
    pyautogui.click(761, 743)
    time.sleep(1)
    pyautogui.click(634,643)
    time.sleep(1)
    pyautogui.click(270,16)
    time.sleep(1)
    pyautogui.click(413,50)
    time.sleep(1)
    pyautogui.typewrite('https://www.easiloan.com/')
    time.sleep(1)
    pyautogui.press('enter')
    time.sleep(3)
    pyautogui.click(1187,146)
    time.sleep(1)
    pyautogui.click(361,439)
    time.sleep(1)
    pyautogui.typewrite('8369887988')
    time.sleep(1)
    pyautogui.click(568,439)
    time.sleep(1)
    enterOTP()
    pyautogui.click(302,376)
    time.sleep(1)

    pyautogui.click(405,496)
    time.sleep(1)
    
    holdBackspace(2)

    pyautogui.typewrite('3,80,000')
    time.sleep(1)
    
    pyautogui.click(616,495)
    holdBackspace(2)
    pyautogui.typewrite('10')
    time.sleep(1)

    pyautogui.click(952,472)
    time.sleep(1)
    pyautogui.click(882,542)
    time.sleep(1)

    pyautogui.click(427,564)
    time.sleep(1)
    pyautogui.click(348,577)
    time.sleep(1)

    pyautogui.click(666,572)
    holdBackspace(2)
    pyautogui.typewrite('10')
    time.sleep(1)

    pyautogui.click(955,565)
    holdBackspace(2)
    pyautogui.typewrite('10000')
    time.sleep(1)

    pyautogui.click(332,654)
    holdBackspace(2)
    pyautogui.typewrite('5000')
    time.sleep(1)

    pyautogui.click(645,654)
    holdBackspace(2)
    pyautogui.typewrite('2000')
    time.sleep(1)

    pyautogui.scroll(-100)

    pyautogui.click(344,668)
    time.sleep(2)

    #Logout and close tab
    pyautogui.click(1197,146)
    time.sleep(2)
    pyautogui.click(231,17)
    time.sleep(1)



def holdBackspace (hold_time):
    import time, pyautogui
    start = time.time()
    while time.time() - start < hold_time:
        pyautogui.press('backspace')

def enterOTP():
    pyautogui.click(280, 533)
    time.sleep(1)
    otp1 = pyautogui.prompt("Enter Otp1 ")
    time.sleep(1)
    pyautogui.typewrite(otp1)
    time.sleep(1)

    pyautogui.click(340, 601)
    time.sleep(4)


if __name__ == '__main__':
    openChromeWindow()